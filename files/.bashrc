# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

alias sp="ssh 10.111.2.11"
alias rj="ssh 10.211.2.11"
alias aed="sudo dnf update -y && sudo dnf upgrade -y && sudo poweroff"
alias vpn="forticlient vpn connect RTM --user=roger.teixeira --password --save-password --always-up --auto-connect"

source /usr/share/git-core/contrib/completion/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export PS1='[\u@\h \W$(declare -F __git_ps1 &>/dev/null && __git_ps1 " (%s)")]\$ '
export LESS='-X'
export MANPAGER='/usr/bin/less'
export EDITOR='/usr/bin/vim'

if command -v tmux &> /dev/null && [ -z "$TMUX" ]; then
    tmux attach -t roger || tmux new -s roger
fi

stty -ixon
