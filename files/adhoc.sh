#!/bin/bash
ansible all -m user -a "name=automation state=present password='{{ 'devops' | password_hash('sha512', 'mysecretsalt') }}'" -b
ansible all -m authorized_key -a "user=automation state=present key='{{ lookup('file', '/home/automation/.ssh/id_rsa.pub') }}'" -b
ansible all -m copy -a "content='automation ALL=(ALL) NOPASSWD: ALL' dest=/etc/sudoers.d/automation" -b
