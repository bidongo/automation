autocmd FileType yaml setlocal ai ts=2 sw=2 et nu cuc
autocmd FileType yaml colo desert
set nowrap
set t_ti= t_te=
set paste
